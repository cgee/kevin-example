// stdlib functionality
#include <iostream>

// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>

// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
//#include "xAODEgamma/EgammaContainer.h"

using namespace std;

int main() {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  cout << "N entries: " << numEntries << endl;
  
  // initialize histograms
  TH1D* h_nJets   = new TH1D("h_nJets", "Number of Jets", 20, 0, 20);
  TH1D* h_nMuon   = new TH1D("h_nMoun", "Number of Muons", 10, 0, 10);
  TH1D* h_nEGamma = new TH1D("h_nEGamma", "Number of Electrons", 10, 0, 10);
  TH1D* h_mjj     = new TH1D("h_mjj",   "Dijet Invariant Mass", 100, 0, 500);

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    //std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;
    
    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    //const xAOD::EgammaContainer* electrons = nullptr;
    //event.retrieve(electrons, "AntiKt4EMTopoJets");

    const xAOD::MuonContainer* muons = nullptr;
    event.retrieve(muons, "Muons");


    h_nMuon->Fill(muons->size());
    //h_nEGamma->Fill(electrons->size());



    std::vector<xAOD::Jet> jets_raw;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event
      //std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
      if (std::abs(jet->eta())<2.5 && jet->pt()>= 60) 
	jets_raw.push_back(*jet);
    }

    h_nJets->Fill(jets_raw.size());
    if (jets_raw.size() > 1)
      h_mjj->Fill((jets_raw.at(0).p4()+jets_raw.at(1).p4()).M()/1000.);

    // counter for the number of events analyzed thus far
    count += 1;
    if (count % 1000 == 0) cout << "Processing event " << count << endl;
  }


  // write out histograms to file
  TFile *MyFile = new TFile("Analysis_hists.root", "RECREATE");
  
  MyFile->cd();

  h_nJets->GetXaxis()->SetTitle("N Jets Per Event");
  h_nJets->GetYaxis()->SetTitle("N Events/Bin");
  
  h_nMuon->GetXaxis()->SetTitle("N Muons Per Event");
  h_nMuon->GetXaxis()->SetTitle("N Events/Bin");

  h_nEGamma->GetXaxis()->SetTitle("N Electrons Per Event");
  h_nEGamma->GetYaxis()->SetTitle("N Events/Bin");

  h_mjj->GetXaxis()->SetTitle("Dijet Invariant Mass [GeV]");
  h_mjj->GetYaxis()->SetTitle("N Events/5 Gev");

  h_nJets  ->Write();
  h_nMuon  ->Write();
  h_nEGamma->Write();
  h_mjj    ->Write();
  
  MyFile->Close();

  // exit from the main function cleanly
  return 0;
}
